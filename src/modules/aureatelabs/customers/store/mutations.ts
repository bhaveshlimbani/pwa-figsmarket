import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.CUSTOMER_FETCH_TOKENS] (state, tokens) {
    state.tokens = tokens || []
  },
  [types.CUSTOMER_STORE_TOKEN] (state, token) {
    state.token = token || []
  }
}
