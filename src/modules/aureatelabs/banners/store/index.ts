import { Module } from 'vuex'
import BannerState from '../types/BannerState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const BannerModuleStore: Module<BannerState, any> = {
  namespaced: true,
  state: {
    banners: []
  },
  mutations,
  actions,
  getters
}
