import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.MYFATOORAH_INITIATE_PAYMENT] (state, methods) {
    state.methods = methods || []
  },
  [types.MYFATOORAH_EXECUTE_PAYMENT] (state, invoice) {
    state.invoice = invoice || []
  },
  [types.MYFATOORAH_DIRECT_PAYMENT] (state, response) {
    state.response = response || []
  },
  [types.MYFATOORAH_CC_INFO] (state, ccdata) {
    state.ccdata = ccdata || []
  }
}
