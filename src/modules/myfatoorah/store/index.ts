
import { Module } from 'vuex'
import { actions } from './actions'
import MyFatoorahState from '../types/MyFatoorahState'
import { mutations } from './mutations'
import { getters } from './getters'
import RootState from '@vue-storefront/core/types/RootState'

export const MyFatoorahModuleStore: Module<MyFatoorahState, RootState> = {
  namespaced: true,
  state: {
    methods: [],
    ccdata: {
      paymentMethodId: 0,
      paymentType: '',
      cardNumber: '',
      cardExpiryMonth: '',
      cardExpiryYear: '',
      cardSecurityCode: 0,
      cardHolderName: '',
      apiURL: ''
    },
    invoice: [],
    response: []
  },
  mutations,
  actions,
  getters
}
